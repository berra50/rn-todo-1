// Import the functions you need from the SDKs you need
import * as firebase from "firebase";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBfieUqrIG_tG7xRfRfqYDNSJSoq9d6ODE",
  authDomain: "rn-todo-289ef.firebaseapp.com",
  projectId: "rn-todo-289ef",
  storageBucket: "rn-todo-289ef.appspot.com",
  messagingSenderId: "423297608231",
  appId: "1:423297608231:web:b2a8ee53a11160777f7c02",
  measurementId: "G-QPJFK3J28Y"
};

// Initialize Firebase
let app;
if(firebase.apps.length === 0){
    app = firebase.initializeApp(firebaseConfig);
}else {
    app = firebase.app();
}

const auth = firebase.auth();
const db = firebase.firestore();

export { auth, db };